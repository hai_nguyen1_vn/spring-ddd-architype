### How to use ###
1. Clone repository
2. Run command to install archetype

	```
	mvn install
	
	```
3. Run command to generate project

	```
	mvn archetype:generate \
		-Dversion=1.0 \
	 	-DgroupId=com.ascendcorp \
		-DartifactId=customer\
		-Dpackage=com.ascendcorp.customer \
		-DarchetypeArtifactId=spring-ddd-archetype \
		-DarchetypeGroupId=com.ascendcorp \
		-DarchetypeVersion=1.0

	```
	

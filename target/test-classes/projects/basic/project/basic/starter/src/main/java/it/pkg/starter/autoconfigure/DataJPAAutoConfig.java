package it.pkg.starter.autoconfigure;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
//@EnableJpaRepositories(basePackages = {"it.pkg.infrastructure.jpa"})
//@EntityScan(basePackages = {"it.pkg.infrastructure.entity"})
public class DataJPAAutoConfig {

}

package it.pkg.starter.config;

import it.pkg.common.annotation.MessageSource;
import it.pkg.common.message.ApplicationMessage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.type.filter.AnnotationTypeFilter;

@Configuration
public class MessageSourceConfig {

  private static final Logger LOGGER = LoggerFactory.getLogger(MessageSourceConfig.class);

  @Bean
  public List<ApplicationMessage> messageList() {
    List<ApplicationMessage> applicationMessages = new ArrayList<>();
    ClassPathScanningCandidateComponentProvider provider = createComponentScanner();
    for (BeanDefinition beanDef : provider
        .findCandidateComponents("com.ascendcorp.migration.service.common.message")) {
      List<ApplicationMessage> applicationMessage = Arrays
          .asList(this.getApplicationMessage(beanDef));

      applicationMessages.addAll(applicationMessage);
    }
    return applicationMessages;
  }

  private ClassPathScanningCandidateComponentProvider createComponentScanner() {
    ClassPathScanningCandidateComponentProvider provider
        = new ClassPathScanningCandidateComponentProvider(false);
    provider.addIncludeFilter(new AnnotationTypeFilter(MessageSource.class));
    return provider;
  }

  private ApplicationMessage[] getApplicationMessage(BeanDefinition beanDef) {
    ApplicationMessage[] returnMessage = new ApplicationMessage[]{};
    try {
      Class<?> cl = Class.forName(beanDef.getBeanClassName());
      boolean messageSource = cl.isAnnotationPresent(MessageSource.class);
      if (messageSource && cl.isEnum()) {
        LOGGER.info("Found class: {} for message source", cl.getSimpleName());
        returnMessage = (ApplicationMessage[]) cl.getEnumConstants();
      }
    } catch (Exception e) {
      LOGGER.error("message source error {} ", e.getMessage());
    }
    return returnMessage;
  }


}

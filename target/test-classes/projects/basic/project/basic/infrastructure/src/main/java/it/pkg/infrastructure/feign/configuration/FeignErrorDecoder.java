package it.pkg.infrastructure.feign.configuration;

import it.pkg.common.exception.ApplicationException;
import it.pkg.common.exception.FeignException;
import it.pkg.common.exception.models.FeignResponse;
import it.pkg.common.message.FeignMessage;
import it.pkg.common.message.InternalServerMessage;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Response;
import feign.codec.ErrorDecoder;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignErrorDecoder implements ErrorDecoder {

  private final ObjectMapper mapper;

  @Autowired
  public FeignErrorDecoder(ObjectMapper objectMapper) {
    this.mapper = objectMapper;
  }

  @Override
  public Exception decode(String methodKey, Response response) {
    FeignResponse.FeignResponseBuilder feignResponseBuilder = FeignResponse.builder()
        .headers(response.headers()).statusCode(response.status());
    try (InputStream inputStream = response.body().asInputStream()) {
      Map<String, Object> bodyResponse = mapper
          .readValue(inputStream, new TypeReference<Map<String, Object>>() {
          });
      feignResponseBuilder.body(bodyResponse);
      FeignResponse feignResponse = feignResponseBuilder.build();
      return new FeignException("feign error: " + feignResponse, FeignMessage.FEIGN_ERROR,
          feignResponse);
    } catch (IOException e) {
      return new ApplicationException(e, InternalServerMessage.INTERNAL_SERVER_ERROR);
    }

  }
}


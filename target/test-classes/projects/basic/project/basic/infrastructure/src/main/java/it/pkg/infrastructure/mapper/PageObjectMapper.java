package it.pkg.infrastructure.mapper;

import it.pkg.domain.model.PageObject;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.data.domain.Page;

@Mapper(injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface PageObjectMapper {

  @InheritInverseConfiguration
  default PageObject toPageObject(Page page) {
    int currentPage = page.getNumber() + 1;
    if (currentPage <= page.getTotalPages()) {
      return PageObject.builder()
          .currentPage(page.getNumber() + 1)
          .hasNext(page.hasNext()).hasPrevious(page.hasPrevious())
          .totalElements(page.getTotalElements())
          .totalPages(page.getTotalPages()).build();
    } else {
      return PageObject.builder()
          .currentPage(currentPage)
          .hasNext(false)
          .hasPrevious(false)
          .totalElements(page.getTotalElements())
          .totalPages(page.getTotalPages())
          .build();
    }
  }
}

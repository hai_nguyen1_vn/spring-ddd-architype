package it.pkg.infrastructure.entity.base;

import java.io.Serializable;
import javax.persistence.MappedSuperclass;
import lombok.EqualsAndHashCode;

@MappedSuperclass
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public abstract class AbstractBaseEntity implements Serializable {

  private static final long serialVersionUID = 2327493874200573956L;
}

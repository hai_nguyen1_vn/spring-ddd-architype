package it.pkg.infrastructure.mapper.entity;

import it.pkg.domain.model.base.AbstractBaseDomainObject;
import it.pkg.infrastructure.entity.base.AbstractBaseEntity;
import java.io.Serializable;

public interface DataMapper<D extends AbstractBaseDomainObject, E extends AbstractBaseEntity> extends
    Serializable {

  E transform(D domain);

  D transform(E entity);
}

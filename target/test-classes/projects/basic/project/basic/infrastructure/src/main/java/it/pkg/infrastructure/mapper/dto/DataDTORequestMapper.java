package it.pkg.infrastructure.mapper.dto;

import it.pkg.domain.model.base.AbstractBaseDomainObject;
import it.pkg.infrastructure.dto.base.AbstractBaseClientDTO;
import java.io.Serializable;

public interface DataDTORequestMapper<D extends AbstractBaseDomainObject, E extends AbstractBaseClientDTO> extends
    Serializable {

  E transform(D domain);
}

package it.pkg.infrastructure.dto;

import it.pkg.infrastructure.dto.base.AbstractBaseClientDTO;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import java.time.LocalDate;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.Value;

@Value
@ToString
@Getter
@Builder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(force = true)
public class DateClientDTO extends AbstractBaseClientDTO {

  private static final long serialVersionUID = -6532062643245329609L;
  @JsonValue
  private LocalDate value;

  @JsonCreator
  public DateClientDTO(LocalDate value) {
    this.value = value;
  }
}

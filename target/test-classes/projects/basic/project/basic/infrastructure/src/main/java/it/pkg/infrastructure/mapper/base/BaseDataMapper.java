package it.pkg.infrastructure.mapper.base;

import it.pkg.domain.model.Date;
import it.pkg.domain.model.Timestamp;
import it.pkg.infrastructure.dto.DateClientDTO;
import it.pkg.infrastructure.dto.TimestampClientDTO;
import it.pkg.infrastructure.entity.TimestampEntity;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface BaseDataMapper {

  Timestamp toTimestamp(TimestampEntity timestampEntity);

  TimestampEntity toTimestampEntity(Timestamp timestamp);

  Timestamp toTimestampFromtDTO(TimestampClientDTO timestampClientDTO);

  DateClientDTO toDateClientDTO(Date date);

}

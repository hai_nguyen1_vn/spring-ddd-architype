package it.pkg.infrastructure.mapper.entity;

import java.io.Serializable;

public interface DataMapperIdentifiable extends Serializable {

  DataMapper get(Object object);
}

package it.pkg.infrastructure.dto.base;

import java.io.Serializable;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
public abstract class AbstractBaseClientDTO implements Serializable {

  private static final long serialVersionUID = -4807682918677756983L;
}

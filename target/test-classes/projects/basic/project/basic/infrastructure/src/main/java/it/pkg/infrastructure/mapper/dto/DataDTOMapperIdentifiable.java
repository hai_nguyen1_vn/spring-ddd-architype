package it.pkg.infrastructure.mapper.dto;

import java.io.Serializable;

public interface DataDTOMapperIdentifiable extends Serializable {

  DataDTOMapper get(Object object);
}

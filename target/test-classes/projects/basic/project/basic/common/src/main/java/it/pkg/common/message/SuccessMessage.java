package it.pkg.common.message;

import it.pkg.common.annotation.MessageSource;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
@MessageSource(name = "SuccessMessage")
public enum SuccessMessage implements ApplicationMessage {
  SUCCESS_MESSAGE("success", "Success");

  private String message;
  private String description;

  @Override
  public int getStatusCode() {
    return 200;
  }

  @Override
  public String getKey() {
    return this.name();
  }

}

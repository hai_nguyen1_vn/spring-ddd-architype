package it.pkg.common.message;

import it.pkg.common.annotation.MessageSource;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
@MessageSource(name = "BadRequestMessage")
public enum BadRequestMessage implements ApplicationMessage {
  INVALID_REQUEST_ERROR("invalid_request", "Invalid request");

  private String message;
  private String description;

  @Override
  public String getKey() {
    return this.name();
  }

  @Override
  public int getStatusCode() {
    return 400;
  }
}

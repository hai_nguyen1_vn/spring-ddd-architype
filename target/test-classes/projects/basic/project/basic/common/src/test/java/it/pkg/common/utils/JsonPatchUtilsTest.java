package it.pkg.common.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import java.io.IOException;
import java.util.Optional;
import org.junit.Test;

public class JsonPatchUtilsTest {

  @Test
  @SuppressWarnings("unchecked")
  public void test__patch__success__shouldReturnCorrectObjectPatch() throws IOException {
    ObjectMapper mapper = new ObjectMapper();
    String json = "[{ \"op\": \"replace\", \"path\": \"/id\", \"value\": \"TEST\" }]";
    JsonNode jsonNode = mapper.readValue(json, JsonNode.class);
    JsonPatch jsonPatch = JsonPatch.fromJson(jsonNode);
    MockJsonObject mockObject = new MockJsonObject();
    assertThatCode(() -> {
      Optional<MockJsonObject> result = JsonPatchUtils.patch(mapper, jsonPatch, mockObject);

      assertThat(result).isNotEmpty();
      assertThat(result.get().getId()).isEqualTo("TEST");
    }).doesNotThrowAnyException();

  }
}
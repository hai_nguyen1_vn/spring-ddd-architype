package it.pkg.common.exception;

import it.pkg.common.exception.models.FeignResponse;
import it.pkg.common.message.ApplicationMessage;
import it.pkg.common.message.FeignMessage;
import lombok.Getter;
import lombok.Getter;

@Getter
public class FeignException extends ApplicationException {

  private static final long serialVersionUID = -6731625104515575513L;
  private final transient FeignResponse response;

  public FeignException(ApplicationMessage error, FeignResponse response) {
    super(error);
    this.response = response;
  }

  public FeignException(Throwable cause, ApplicationMessage error, FeignResponse response) {
    super(cause, error);
    this.response = response;
  }

  public FeignException(String exceptionMessage, ApplicationMessage error,
      FeignResponse response) {
    super(exceptionMessage, error);
    this.response = response;
  }

  public FeignException(FeignResponse response) {
    super(FeignMessage.FEIGN_ERROR);
    this.response = response;
  }
}


package it.pkg.common.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.stream.Stream;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class EncryptionUtils {

  private static final Logger LOGGER = LoggerFactory.getLogger(EncryptionUtils.class);
  private static final String CIPHER = "RSA/ECB/PKCS1Padding";

  public static String decryptByPrivateKey(String encryptedText, String privateKey) {
    try {
      byte[] bytes = Base64.getDecoder().decode(encryptedText);
      Cipher cipher = Cipher.getInstance(CIPHER);
      cipher.init(Cipher.DECRYPT_MODE, readPrivateKey(privateKey));
      byte[] decryptedText = cipher.doFinal(bytes);
      return new String(decryptedText, StandardCharsets.UTF_8);
    } catch (NoSuchAlgorithmException | NoSuchProviderException | BadPaddingException | IllegalBlockSizeException |
        InvalidKeySpecException | InvalidKeyException | IOException | NoSuchPaddingException e) {
      LOGGER.error("Decrypt error", e);
      return null;
    }
  }

  public static String md5(String plainText) {
    try {
      MessageDigest md5 = MessageDigest.getInstance("MD5");
      byte[] digest = md5.digest(plainText.getBytes(StandardCharsets.UTF_8));
      StringBuilder sb = new StringBuilder();
      for (byte b : digest) {
        sb.append(Integer.toHexString((b & 0xFF) | 0x100), 1, 3);
      }
      return sb.toString();
    } catch (Exception e) {
      return null;
    }
  }

  public static String encryptByPublicKey(String content, String publicKey) {
    try {
      byte[] publicBytes = readPemObject(publicKey).getContent();
      X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);
      KeyFactory keyFactory = KeyFactory.getInstance("RSA");
      PublicKey pubKey = keyFactory.generatePublic(keySpec);
      byte[] contentBytes = content.getBytes(StandardCharsets.UTF_8);
      Cipher cipher = Cipher.getInstance(CIPHER);
      cipher.init(Cipher.ENCRYPT_MODE, pubKey);
      byte[] cipherContent = cipher.doFinal(contentBytes);
      return Base64.getEncoder().encodeToString(cipherContent);
    } catch (InvalidKeySpecException | IllegalBlockSizeException | IOException | NullPointerException |
        NoSuchPaddingException | InvalidKeyException | BadPaddingException | NoSuchAlgorithmException e) {
      LOGGER.error("Encrypt error", e);
      return null;
    }
  }

  public static String encryptByPublicKeyFile(String content, Resource resource) {
    try (Stream<String> stream = Files
        .lines(Paths.get(resource.getURI()), StandardCharsets.UTF_8)) {
      StringBuilder contentBuilder = new StringBuilder();
      stream.forEach(s -> contentBuilder.append(s).append("\n"));
      return encryptByPublicKey(content, contentBuilder.toString());
    } catch (Exception e) {
      LOGGER.error("Decrypt by file error", e);
      return null;
    }
  }

  private static PrivateKey readPrivateKey(String privateKey)
      throws InvalidKeySpecException, IOException, NoSuchAlgorithmException, NoSuchProviderException {
    if (Security.getProvider("BC") == null) {
      Security.addProvider(new BouncyCastleProvider());
    }
    byte[] keyBytes = readPemObject(privateKey).getContent();
    PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
    KeyFactory kf = KeyFactory.getInstance("RSA", "BC");
    return kf.generatePrivate(spec);
  }

  private static PemObject readPemObject(String data) throws IOException {
    byte[] bytes = data.getBytes(StandardCharsets.UTF_8);
    try (PemReader pemReader = new PemReader(
        new InputStreamReader(new ByteArrayInputStream(bytes), StandardCharsets.UTF_8))) {
      return pemReader.readPemObject();
    }
  }
}

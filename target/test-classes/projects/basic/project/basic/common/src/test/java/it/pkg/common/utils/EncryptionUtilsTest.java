package it.pkg.common.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import org.junit.Test;
import org.springframework.core.io.Resource;

public class EncryptionUtilsTest {

  private final static String TEST_RSA_PRIVATE_KEY = "-----BEGIN RSA PRIVATE KEY-----\n" +
      "MIICXQIBAAKBgQCsfOnDzDh8d7ZvlMnXZaZhVZ5hrU85VhXCAGQNokTVw6NvHbv0\n" +
      "sOJF6ttehfsDlokpAy6SMsfuOK2pXVzV6Y4PcFgl8CO5foxv0Xw0BjP8Le6NhDSF\n" +
      "fJGzzLNxBiNDtSuE+GSRzggyF/a8YyJP2xOTBEM4Owdoyv4YmjBnl5L0xwIDAQAB\n" +
      "AoGBAKjzXYIUiguYstDOm3npLjROyekA+gW+RWeWPGqCVAxSKcaQCGefzrMPXTpT\n" +
      "38/e5pCOdlJrfRvg1nF7apB4yVTAlGUWjqTr3uFSKKZVjoXqpqOgAeBAqkRJcTsk\n" +
      "0RBysjqO/+5yEKK+8l0Ofsg+39bEX3BBwZg8MVMhudJ8g+xJAkEA/yh9zEQ55NXJ\n" +
      "vqffkWRcdY9GDN3YxRVrF9P7kJTBv/46+29X6LohOQgui/mQFplwuPX9vyDqm6mx\n" +
      "IIUFLOflawJBAK0OmQq2x4FrZh1BDE8dACwyWXw4yrAtbQ7eD4nSngCKOUcQgZBI\n" +
      "9kjgPLrJDwaeBvOmMI2biPUOIX14XpMtKRUCQAK2sofOnfMCFxAxBt6r+5PAf1U5\n" +
      "ssl9zdLGDWHfQyRAlu3/pCa0fA/4N06Dy/WBkkJVU2qJ9hTLvDeFUqXEnZsCQBLg\n" +
      "ALk2blQjTqPqMFmApEAtzazK1PCaQ8bXWYKCwlD0woKJvlfqXVJdgsIso8LpAYEZ\n" +
      "ozoOuMVhoS16L3aF+nECQQCeHuJQDosOxI4idJxgA9LrctP4gtJCksGqgTa4Pkyi\n" +
      "WCeF9ljDkZ8OtgP9X2veEDfm+UwoctHT0a5PPZ+rFdIP\n" +
      "-----END RSA PRIVATE KEY-----";
  private final static String TEST_RSA_PUBLIC_KEY = "-----BEGIN PUBLIC KEY-----\n" +
      "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCsfOnDzDh8d7ZvlMnXZaZhVZ5\n" +
      "hrU85VhXCAGQNokTVw6NvHbv0sOJF6ttehfsDlokpAy6SMsfuOK2pXVzV6Y4PcF\n" +
      "gl8CO5foxv0Xw0BjP8Le6NhDSFfJGzzLNxBiNDtSuE+GSRzggyF/a8YyJP2xOTB\n" +
      "EM4Owdoyv4YmjBnl5L0xwIDAQAB\n" +
      "-----END PUBLIC KEY-----";

  @Test
  public void test__encryptByPublicKey__success__returnNotNullEncryptedText() {
    String result = EncryptionUtils.encryptByPublicKey("test", TEST_RSA_PUBLIC_KEY);
    assertThat(result).isNotNull();
    assertThat(result).isNotBlank();
  }

  @Test
  public void test__decryptByPrivateKey__success__returnPlainText() {
    String encryptedText = EncryptionUtils.encryptByPublicKey("test", TEST_RSA_PUBLIC_KEY);
    String plainText = EncryptionUtils.decryptByPrivateKey(encryptedText, TEST_RSA_PRIVATE_KEY);
    assertThat(plainText).isNotNull();
    assertThat(plainText).isNotBlank();
    assertThat(plainText).isEqualTo("test");
  }

  @Test
  public void test__decryptByPrivateKey__success__returnNull() {
    String encryptedText = EncryptionUtils.encryptByPublicKey("test", TEST_RSA_PUBLIC_KEY);
    String plainText = EncryptionUtils.decryptByPrivateKey(encryptedText, TEST_RSA_PUBLIC_KEY);
    assertThat(plainText).isNull();
  }

  @Test
  public void test__encryptByPublicKey__fail__returnNull() {
    String result = EncryptionUtils.encryptByPublicKey("test", TEST_RSA_PRIVATE_KEY);
    assertThat(result).isNull();
  }

  @Test
  public void test__md5__success__shouldReturnCorrectString() {
    String result = EncryptionUtils.md5("test");
    assertThat(result).isNotNull();
    assertThat(result).isEqualTo("098f6bcd4621d373cade4e832627b4f6");
  }

  @Test
  public void test__md5__faile__shouldReturnNull() {
    String result = EncryptionUtils.md5(null);
    assertThat(result).isNull();
  }

  @Test
  public void test__encryptByPublicKeyFile__success__returnNotNullEncryptedText()
      throws IOException, URISyntaxException {
    Resource resource = mock(Resource.class);
    URL url = this.getClass().getClassLoader().getResource("test-key.txt");
    when(resource.getURI()).thenReturn(url.toURI());

    String result = EncryptionUtils.encryptByPublicKeyFile("test", resource);
    assertThat(result).isNotNull();
    assertThat(result).isNotBlank();
  }

  @Test
  public void test__encryptByPublicKeyFile__fail__returnNullEncryptedText()
      throws IOException, URISyntaxException {
    Resource resource = mock(Resource.class);
    URL url = this.getClass().getClassLoader().getResource("test-key-fail.txt");
    when(resource.getURI()).thenReturn(url.toURI());

    String result = EncryptionUtils.encryptByPublicKeyFile("test", resource);
    assertThat(result).isNull();
  }
}
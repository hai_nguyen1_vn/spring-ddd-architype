package it.pkg.common.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Value;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Value
public class PageConstant {

  public static final String PAGE_INDEX = "page_index";
  public static final String PAGE_SIZE = "page_size";
  public static final String PAGING = "paging";
}

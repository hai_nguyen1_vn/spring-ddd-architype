package it.pkg.common.utils;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MockJsonObject {

  @JsonProperty("id")
  private String id;

  public MockJsonObject() {
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }
}

package it.pkg.common.message;

import it.pkg.common.annotation.MessageSource;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
@MessageSource(name = "FeignMessage")
public enum FeignMessage implements ApplicationMessage {
  FEIGN_ERROR("feign_error", "Feign error");

  private String message;
  private String description;

  @Override
  public int getStatusCode() {
    return 400;
  }

  @Override
  public String getKey() {
    return this.name();
  }

}

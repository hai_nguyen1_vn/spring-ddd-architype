package it.pkg.presentation.mapper;

import it.pkg.domain.model.base.AbstractBaseDomainObject;
import java.io.Serializable;

public interface ResponseMapper<D extends AbstractBaseDomainObject, E extends Serializable> extends
    Serializable {

  E transform(D domain);
}

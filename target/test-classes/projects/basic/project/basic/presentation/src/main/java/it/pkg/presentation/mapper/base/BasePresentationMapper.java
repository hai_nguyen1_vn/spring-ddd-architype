package it.pkg.presentation.mapper.base;

import it.pkg.domain.model.Timestamp;
import it.pkg.presentation.dto.TimestampDTO;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface BasePresentationMapper {

  Timestamp toTimestamp(TimestampDTO timestamp);

  TimestampDTO toTimestampDTO(Timestamp timestamp);

}

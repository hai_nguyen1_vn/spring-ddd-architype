package it.pkg.presentation.controller;

import it.pkg.common.exception.ApplicationException;
import it.pkg.common.message.ApplicationMessage;
import it.pkg.common.exception.FeignException;
import it.pkg.common.message.BadRequestMessage;
import it.pkg.common.message.InternalServerMessage;
import it.pkg.presentation.utils.ResponseFactory;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.util.StringUtils;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerController extends BaseController {

  private List<ApplicationMessage> messageList;

  @Autowired
  public ExceptionHandlerController(
      List<ApplicationMessage> messageList) {
    this.messageList = messageList;
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity handleException(Exception ex) {
    logger.error(
        "Application error", ex);
    return ResponseFactory
        .error(HttpStatus.INTERNAL_SERVER_ERROR, InternalServerMessage.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(ApplicationException.class)
  public ResponseEntity handleApplicationException(ApplicationException e) {
    HttpStatus httpStatus = HttpStatus.valueOf(e.getError().getStatusCode());
    return ResponseFactory.error(httpStatus, e.getError().getMessage(), e.getMessage());
  }

  @ExceptionHandler({HttpMessageNotReadableException.class, MissingRequestHeaderException.class})
  public ResponseEntity handleBadRequestException(final HttpServletRequest request, Exception ex) {
    logger.error(
        "Request with request url [{}] method [{}] has invalid headers or body",
        request.getRequestURI(), request.getMethod(), ex);
    HttpStatus httpStatus = HttpStatus
        .valueOf(BadRequestMessage.INVALID_REQUEST_ERROR.getStatusCode());
    return ResponseFactory.error(httpStatus, BadRequestMessage.INVALID_REQUEST_ERROR);
  }

  @ExceptionHandler({FeignException.class})
  public ResponseEntity handleFeignException(FeignException e) {
    logger.error("Feign response {}", e.getResponse());
    HttpStatus httpStatus = HttpStatus
        .valueOf(InternalServerMessage.INTERNAL_SERVER_ERROR.getStatusCode());
    return ResponseFactory.error(httpStatus, InternalServerMessage.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity handleMethodArgumentNotValidException(final HttpServletRequest request,
      final MethodArgumentNotValidException e) {
    FieldError error = e.getBindingResult().getFieldErrors().get(0);

    logger.error(
        "Request with request url [{}] method [{}] has invalid argument with field [{}] check type [{}]",
        request.getRequestURI(), request.getMethod(), error.getField(), error.getCode());
    String messageKey = error.getDefaultMessage();
    HttpStatus httpStatus = HttpStatus
        .valueOf(BadRequestMessage.INVALID_REQUEST_ERROR.getStatusCode());
    if (StringUtils.isEmpty(messageKey) || !messageKey.startsWith("@")) {
      return ResponseFactory.error(httpStatus, BadRequestMessage.INVALID_REQUEST_ERROR);
    }
    ApplicationMessage applicationMessage = this.getApplicationMessage(messageKey);
    if (applicationMessage == null) {
      return ResponseFactory.error(httpStatus, BadRequestMessage.INVALID_REQUEST_ERROR);
    }
    httpStatus = HttpStatus
        .valueOf(applicationMessage.getStatusCode());
    String message = applicationMessage.getDescription()
        .replace("{validatedField}", error.getField());

    return ResponseFactory.error(httpStatus, applicationMessage.getMessage(), message);
  }

  private ApplicationMessage getApplicationMessage(String key) {
    for (ApplicationMessage message : messageList) {
      ApplicationMessage returnMessage = message.get(key.substring(1));
      if (returnMessage != null) {
        return returnMessage;
      }
    }
    return null;
  }
}

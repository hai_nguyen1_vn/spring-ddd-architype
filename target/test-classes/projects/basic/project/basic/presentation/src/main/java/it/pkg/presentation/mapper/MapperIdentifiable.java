package it.pkg.presentation.mapper;

import java.io.Serializable;

public interface MapperIdentifiable extends Serializable {

  ResponseMapper get(Object object);
}

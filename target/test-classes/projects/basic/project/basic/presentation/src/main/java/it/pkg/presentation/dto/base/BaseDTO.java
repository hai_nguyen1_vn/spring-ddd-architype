package it.pkg.presentation.dto.base;

import java.io.Serializable;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public abstract class BaseDTO implements Serializable {

  private static final long serialVersionUID = -7135180293570635376L;
}

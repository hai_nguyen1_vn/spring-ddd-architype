package it.pkg.presentation.mapper;

import it.pkg.domain.model.PageObject;
import it.pkg.presentation.dto.response.PageResponse;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface PagePresentationMapper {

  PageResponse toPageResponse(PageObject pageObject);

}

package it.pkg.presentation.dto;

import it.pkg.presentation.dto.base.BaseDTO;
import com.fasterxml.jackson.annotation.JsonValue;
import java.time.Instant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;

@Value
@EqualsAndHashCode(callSuper = true)
@ToString
@Builder
@AllArgsConstructor
public class TimestampDTO extends BaseDTO {

  private static final long serialVersionUID = -123866390235073415L;
  @JsonValue
  private Instant value;

  @SuppressWarnings("unused")
  public TimestampDTO() {
    this.value = null;
  }
}

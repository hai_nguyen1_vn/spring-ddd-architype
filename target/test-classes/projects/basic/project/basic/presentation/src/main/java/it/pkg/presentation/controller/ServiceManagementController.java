package it.pkg.presentation.controller;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthEndpoint;
import org.springframework.boot.actuate.info.InfoEndpoint;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServiceManagementController {

  private final HealthEndpoint healthEndpoint;
  private final InfoEndpoint infoEndpoint;

  @Autowired
  public ServiceManagementController(HealthEndpoint healthEndpoint,
      InfoEndpoint infoEndpoint) {
    this.healthEndpoint = healthEndpoint;
    this.infoEndpoint = infoEndpoint;
  }

  @GetMapping("health")
  public Health getHealth() {
    return healthEndpoint.health();
  }

  @GetMapping("info")
  public Map<String, Object> getInfo() {
    return infoEndpoint.info();
  }
}


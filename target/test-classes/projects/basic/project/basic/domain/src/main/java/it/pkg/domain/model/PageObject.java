package it.pkg.domain.model;

import it.pkg.domain.model.base.AbstractBaseDomainObject;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.Value;

@Value
@Builder
@Getter
@ToString
@EqualsAndHashCode(callSuper = true)
public class PageObject extends AbstractBaseDomainObject {

  private int totalPages;
  private boolean hasNext;
  private boolean hasPrevious;
  private int currentPage;
  private long totalElements;
}

package it.pkg.domain.model.base;

import lombok.EqualsAndHashCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public abstract class AbstractBaseDomainObject {

  protected final Logger logger = LoggerFactory.getLogger(getClass());
}

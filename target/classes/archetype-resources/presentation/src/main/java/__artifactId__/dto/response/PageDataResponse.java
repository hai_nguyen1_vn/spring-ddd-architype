#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.dto.response;

import ${package}.${artifactId}.serializer.PageDataResponseSerializer;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.util.ArrayList;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@Builder
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonSerialize(using = PageDataResponseSerializer.class)
public class PageDataResponse<T extends Serializable> implements Serializable {

  private static final long serialVersionUID = 155405991017952320L;

  private ArrayList<T> data;
  private PageResponse page;
  @Builder.Default
  private String jsonDataName = "data";

}

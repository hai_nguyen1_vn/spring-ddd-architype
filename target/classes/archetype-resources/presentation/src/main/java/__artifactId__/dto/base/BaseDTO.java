#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.dto.base;

import java.io.Serializable;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public abstract class BaseDTO implements Serializable {

  private static final long serialVersionUID = -7135180293570635376L;
}

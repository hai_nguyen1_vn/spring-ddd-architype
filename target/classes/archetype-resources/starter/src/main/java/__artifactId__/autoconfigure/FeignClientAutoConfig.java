#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.autoconfigure;

import feign.Logger;
import okhttp3.OkHttpClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackages = {"${package}.infrastructure.feign.client"})
public class FeignClientAutoConfig {

  @Bean
  public OkHttpClient.Builder okHttpClientBuilder() {
    return new OkHttpClient.Builder();
  }

  @Bean
  public Logger.Level loggerLevel() {
    return Logger.Level.FULL;
  }
}

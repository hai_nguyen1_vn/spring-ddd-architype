#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.config;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.sleuth.instrument.web.SkipPatternProvider;
import org.springframework.cloud.sleuth.instrument.web.SleuthWebProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ZipkinConfig {

  @Bean
  protected SkipPatternProvider sleuthSkipPatternProvider(
      @Value("${symbol_dollar}{server.servlet.context-path:}") String contextPath) {
    final String defaultSkipPattern =
        SleuthWebProperties.DEFAULT_SKIP_PATTERN + "|/|/csrf|/webjars.*";
    if (StringUtils.isBlank(contextPath)) {
      return () -> Pattern.compile(defaultSkipPattern);
    }

    final String[] defaultPatterns = StringUtils.split(defaultSkipPattern, '|');
    final List<String> patterns = new ArrayList<>(defaultPatterns.length);
    for (final String defaultPattern : defaultPatterns) {
      if (defaultPattern.startsWith("/")) {
        patterns.add(contextPath + defaultPattern);
      } else {
        patterns.add(defaultPattern);
      }
    }
    final String finalPattern = StringUtils.join(patterns, '|');
    return () -> Pattern.compile(finalPattern);
  }
}


#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import java.util.Optional;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class JsonPatchUtils {

  public static <T> Optional patch(ObjectMapper mapper, JsonPatch jsonPatch, T target)
      throws JsonPatchException {
    JsonNode patchedNode = jsonPatch.apply(mapper.convertValue(target, JsonNode.class));
    return Optional.ofNullable(mapper.convertValue(patchedNode, target.getClass()));
  }
}

#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.exception;

import ${package}.${artifactId}.exception.models.FeignResponse;
import ${package}.${artifactId}.message.ApplicationMessage;
import ${package}.${artifactId}.message.FeignMessage;
import lombok.Getter;
import lombok.Getter;

@Getter
public class FeignException extends ApplicationException {

  private static final long serialVersionUID = -6731625104515575513L;
  private final transient FeignResponse response;

  public FeignException(ApplicationMessage error, FeignResponse response) {
    super(error);
    this.response = response;
  }

  public FeignException(Throwable cause, ApplicationMessage error, FeignResponse response) {
    super(cause, error);
    this.response = response;
  }

  public FeignException(String exceptionMessage, ApplicationMessage error,
      FeignResponse response) {
    super(exceptionMessage, error);
    this.response = response;
  }

  public FeignException(FeignResponse response) {
    super(FeignMessage.FEIGN_ERROR);
    this.response = response;
  }
}


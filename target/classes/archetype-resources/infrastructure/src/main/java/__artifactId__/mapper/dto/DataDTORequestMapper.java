#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.mapper.dto;

import ${package}.domain.model.base.AbstractBaseDomainObject;
import ${package}.${artifactId}.dto.base.AbstractBaseClientDTO;
import java.io.Serializable;

public interface DataDTORequestMapper<D extends AbstractBaseDomainObject, E extends AbstractBaseClientDTO> extends
    Serializable {

  E transform(D domain);
}

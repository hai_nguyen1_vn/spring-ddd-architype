#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.entity;

import ${package}.${artifactId}.entity.base.AbstractBaseEntity;
import java.time.Instant;
import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.Value;

@Embeddable
@Value
@ToString
@Getter
@Builder
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class TimestampEntity extends AbstractBaseEntity {

  private static final long serialVersionUID = -13951307647043268L;

  private Instant value;

  @SuppressWarnings("unused")
  public TimestampEntity() {
    this.value = null;
  }
}

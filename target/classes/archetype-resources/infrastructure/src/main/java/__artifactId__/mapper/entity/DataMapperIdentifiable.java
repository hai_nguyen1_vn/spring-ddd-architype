#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.mapper.entity;

import java.io.Serializable;

public interface DataMapperIdentifiable extends Serializable {

  DataMapper get(Object object);
}

#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.mapper.entity;

import ${package}.domain.model.base.AbstractBaseDomainObject;
import ${package}.${artifactId}.entity.base.AbstractBaseEntity;
import java.io.Serializable;

public interface DataMapper<D extends AbstractBaseDomainObject, E extends AbstractBaseEntity> extends
    Serializable {

  E transform(D domain);

  D transform(E entity);
}

#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.mapper.dto;

import java.io.Serializable;

public interface DataDTOMapperIdentifiable extends Serializable {

  DataDTOMapper get(Object object);
}

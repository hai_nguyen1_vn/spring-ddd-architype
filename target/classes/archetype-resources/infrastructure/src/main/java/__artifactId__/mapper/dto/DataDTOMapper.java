#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.mapper.dto;

import ${package}.domain.model.base.AbstractBaseDomainObject;
import ${package}.${artifactId}.dto.base.AbstractBaseClientDTO;
import java.io.Serializable;

public interface DataDTOMapper<D extends AbstractBaseDomainObject, E extends AbstractBaseClientDTO> extends
    Serializable {

  D transform(E dto);
}

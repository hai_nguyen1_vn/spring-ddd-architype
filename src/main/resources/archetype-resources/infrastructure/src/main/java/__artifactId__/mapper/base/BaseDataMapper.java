#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.mapper.base;

import ${package}.domain.model.Date;
import ${package}.domain.model.Timestamp;
import ${package}.${artifactId}.dto.DateClientDTO;
import ${package}.${artifactId}.dto.TimestampClientDTO;
import ${package}.${artifactId}.entity.TimestampEntity;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface BaseDataMapper {

  Timestamp toTimestamp(TimestampEntity timestampEntity);

  TimestampEntity toTimestampEntity(Timestamp timestamp);

  Timestamp toTimestampFromtDTO(TimestampClientDTO timestampClientDTO);

  DateClientDTO toDateClientDTO(Date date);

}

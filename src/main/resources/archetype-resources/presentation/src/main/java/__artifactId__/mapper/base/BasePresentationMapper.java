#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.mapper.base;

import ${package}.domain.model.Timestamp;
import ${package}.${artifactId}.dto.TimestampDTO;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface BasePresentationMapper {

  Timestamp toTimestamp(TimestampDTO timestamp);

  TimestampDTO toTimestampDTO(Timestamp timestamp);

}

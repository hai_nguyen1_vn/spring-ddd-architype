#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.mapper;

import ${package}.domain.model.base.AbstractBaseDomainObject;
import java.io.Serializable;

public interface ResponseMapper<D extends AbstractBaseDomainObject, E extends Serializable> extends
    Serializable {

  E transform(D domain);
}

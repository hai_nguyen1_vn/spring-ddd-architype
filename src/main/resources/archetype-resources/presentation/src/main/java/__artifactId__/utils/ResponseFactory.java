#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.utils;

import ${package}.common.message.ApplicationMessage;
import ${package}.common.message.SuccessMessage;
import ${package}.${artifactId}.dto.response.GeneralResponse;
import ${package}.${artifactId}.dto.response.ResponseStatus;
import java.io.Serializable;
import java.util.List;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ResponseFactory {

  public static <T extends Serializable> ResponseEntity<GeneralResponse<T>> created(T data) {
    ResponseStatus responseStatus = ResponseStatus.builder()
        .code(SuccessMessage.SUCCESS_MESSAGE.getMessage())
        .message(SuccessMessage.SUCCESS_MESSAGE.getDescription()).build();
    GeneralResponse<T> generalResponse = GeneralResponse.<T>builder().data(data)
        .status(responseStatus).build();
    return ResponseEntity.status(HttpStatus.CREATED).body(generalResponse);
  }

  public static <T extends Serializable> ResponseEntity<GeneralResponse<T>> created() {
    ResponseStatus responseStatus = ResponseStatus.builder()
        .code(SuccessMessage.SUCCESS_MESSAGE.getMessage())
        .message(SuccessMessage.SUCCESS_MESSAGE.getDescription()).build();
    GeneralResponse<T> generalResponse = GeneralResponse.<T>builder().status(responseStatus)
        .build();
    return ResponseEntity.status(HttpStatus.CREATED).body(generalResponse);
  }

  public static <T extends Serializable> ResponseEntity<GeneralResponse<T>> success() {
    ResponseStatus responseStatus = ResponseStatus.builder()
        .code(SuccessMessage.SUCCESS_MESSAGE.getMessage())
        .message(SuccessMessage.SUCCESS_MESSAGE.getDescription()).build();
    GeneralResponse<T> generalResponse = GeneralResponse.<T>builder().status(responseStatus)
        .build();
    return ResponseEntity.ok(generalResponse);
  }

  public static <T extends Serializable> ResponseEntity<GeneralResponse<T>> success(T data) {
    ResponseStatus responseStatus = ResponseStatus.builder()
        .code(SuccessMessage.SUCCESS_MESSAGE.getMessage())
        .message(SuccessMessage.SUCCESS_MESSAGE.getDescription()).build();
    GeneralResponse<T> generalResponse = GeneralResponse.<T>builder().data(data)
        .status(responseStatus).build();
    return ResponseEntity.ok(generalResponse);
  }

  public static <T extends Serializable> ResponseEntity<GeneralResponse<T>> success(T data,
      ApplicationMessage message) {
    ResponseStatus responseStatus = ResponseStatus.builder()
        .code(message.getMessage()).message(message.getDescription()).build();
    GeneralResponse<T> generalResponse = GeneralResponse.<T>builder().data(data)
        .status(responseStatus).build();
    return ResponseEntity.status(message.getStatusCode()).body(generalResponse);
  }

  public static ResponseEntity<GeneralResponse> error(HttpStatus httpStatus,
      ApplicationMessage error) {
    ResponseStatus responseStatus = ResponseStatus.builder()
        .code(error.getMessage()).message(error.getDescription()).build();
    GeneralResponse generalResponse = GeneralResponse.builder().status(responseStatus).data(null)
        .build();
    return ResponseEntity.status(httpStatus).body(generalResponse);
  }

  public static ResponseEntity<GeneralResponse> error(HttpStatus httpStatus,
      String code, String message) {
    ResponseStatus responseStatus = ResponseStatus.builder()
        .code(code).message(message).build();
    GeneralResponse generalResponse = GeneralResponse.builder().status(responseStatus).data(null)
        .build();
    return ResponseEntity.status(httpStatus).body(generalResponse);
  }
}

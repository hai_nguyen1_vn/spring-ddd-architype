#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.mapper;

import java.io.Serializable;

public interface MapperIdentifiable extends Serializable {

  ResponseMapper get(Object object);
}

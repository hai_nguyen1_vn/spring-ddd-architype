#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.serializer;

import ${package}.${artifactId}.dto.response.PageDataResponse;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import java.io.Serializable;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PageDataResponseSerializer<T extends Serializable> extends
    JsonSerializer<PageDataResponse<T>> {

  @Override
  public void serialize(PageDataResponse<T> tPageDataResponse, JsonGenerator jsonGenerator,
      SerializerProvider serializerProvider) throws IOException {
    jsonGenerator.writeStartObject();
    jsonGenerator
        .writeObjectField(tPageDataResponse.getJsonDataName(), tPageDataResponse.getData());
    jsonGenerator.writeObjectField("page", tPageDataResponse.getPage());
    jsonGenerator.writeEndObject();
  }
}

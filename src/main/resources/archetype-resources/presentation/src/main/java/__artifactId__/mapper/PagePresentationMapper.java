#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.mapper;

import ${package}.domain.model.PageObject;
import ${package}.${artifactId}.dto.response.PageResponse;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface PagePresentationMapper {

  PageResponse toPageResponse(PageObject pageObject);

}

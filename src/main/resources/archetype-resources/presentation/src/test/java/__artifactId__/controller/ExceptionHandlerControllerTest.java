#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.controller;

import static ${package}.common.message.BadRequestMessage.INVALID_REQUEST_ERROR;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import ${package}.common.exception.ApplicationException;
import ${package}.common.exception.FeignException;
import ${package}.common.exception.models.FeignResponse;
import ${package}.common.message.ApplicationMessage;
import ${package}.common.message.InternalServerMessage;
import ${package}.${artifactId}.dto.response.GeneralResponse;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

@RunWith(MockitoJUnitRunner.class)
public class ExceptionHandlerControllerTest {

  private ExceptionHandlerController exceptionHandlerController;
  private List<ApplicationMessage> applicationMessages = new ArrayList<>();

  @Before
  public void setUp() {
    exceptionHandlerController = new ExceptionHandlerController(applicationMessages);
  }

  @After
  public void tearDown() {
    exceptionHandlerController = null;
    applicationMessages.clear();
  }

  @Test
  public void test__handleException__shouldReturnCorrectResponse() {

    ResponseEntity result = exceptionHandlerController.handleException(new Exception());

    assertThat(result).isNotNull();
    assertThat(result.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    assertThat(result.getBody()).isNotNull();
    assertThat(result.getBody()).isInstanceOf(GeneralResponse.class);
    assertThat(((GeneralResponse) result.getBody()).getStatus().getMessage())
        .isEqualTo(InternalServerMessage.INTERNAL_SERVER_ERROR.getDescription());
    assertThat(((GeneralResponse) result.getBody()).getStatus().getCode())
        .isEqualTo(InternalServerMessage.INTERNAL_SERVER_ERROR.getMessage());
  }


  @Test
  public void test__handleApplicationException__shouldReturnCorrectResponse() {
    ApplicationException internalServerException = new ApplicationException("test exception");

    ResponseEntity result = exceptionHandlerController
        .handleApplicationException(internalServerException);

    assertThat(result).isNotNull();
    assertThat(result.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    assertThat(result.getBody()).isNotNull();
    assertThat(result.getBody()).isInstanceOf(GeneralResponse.class);
    assertThat(((GeneralResponse) result.getBody()).getStatus().getMessage())
        .isEqualTo("test exception");
    assertThat(((GeneralResponse) result.getBody()).getStatus().getCode())
        .isEqualTo(InternalServerMessage.INTERNAL_SERVER_ERROR.getMessage());
  }

  @Test
  public void test__handleBadRequestException__shouldReturnCorrectResponse() {
    HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
    HttpMessageNotReadableException httpMessageNotReadableException = mock(
        HttpMessageNotReadableException.class);

    ResponseEntity result = exceptionHandlerController
        .handleBadRequestException(httpServletRequest, httpMessageNotReadableException);

    assertThat(result).isNotNull();
    assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(result.getBody()).isNotNull();
    assertThat(result.getBody()).isInstanceOf(GeneralResponse.class);
    assertThat(((GeneralResponse) result.getBody()).getStatus().getMessage())
        .isEqualTo(INVALID_REQUEST_ERROR.getDescription());
    assertThat(((GeneralResponse) result.getBody()).getStatus().getCode())
        .isEqualTo(INVALID_REQUEST_ERROR.getMessage());
  }

  @Test
  public void test__handleFeignException__shouldReturnCorrectResponse() {

    FeignException feignException = new FeignException(FeignResponse.builder().build());

    ResponseEntity result = exceptionHandlerController.handleFeignException(feignException);

    assertThat(result).isNotNull();
    assertThat(result.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    assertThat(result.getBody()).isNotNull();
    assertThat(result.getBody()).isInstanceOf(GeneralResponse.class);
    assertThat(((GeneralResponse) result.getBody()).getStatus().getMessage())
        .isEqualTo(InternalServerMessage.INTERNAL_SERVER_ERROR.getDescription());
    assertThat(((GeneralResponse) result.getBody()).getStatus().getCode())
        .isEqualTo(InternalServerMessage.INTERNAL_SERVER_ERROR.getMessage());
  }


  @Test
  public void test__handleMethodArgumentNotValidException__successWithoutKeyAtMessage__shouldReturnCorrectResponse() {
    applicationMessages.add(new CustomMessage());
    HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
    MethodArgumentNotValidException methodArgumentNotValidException = mock(
        MethodArgumentNotValidException.class);
    List<FieldError> fieldErrors = new ArrayList<>();
    FieldError error = mock(FieldError.class);
    when(error.getField()).thenReturn("test");
    when(error.getDefaultMessage()).thenReturn("key");
    fieldErrors.add(error);
    BindingResult bindingResult = mock(BindingResult.class);
    when(bindingResult.getFieldErrors()).thenReturn(fieldErrors);

    when(methodArgumentNotValidException.getBindingResult()).thenReturn(bindingResult);

    ResponseEntity result = exceptionHandlerController
        .handleMethodArgumentNotValidException(httpServletRequest, methodArgumentNotValidException);

    assertThat(result).isNotNull();
    assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(result.getBody()).isNotNull();
    assertThat(result.getBody()).isInstanceOf(GeneralResponse.class);
    assertThat(((GeneralResponse) result.getBody()).getStatus().getMessage())
        .isEqualTo(INVALID_REQUEST_ERROR.getDescription());
    assertThat(((GeneralResponse) result.getBody()).getStatus().getCode())
        .isEqualTo(INVALID_REQUEST_ERROR.getMessage());
  }

  @Test
  public void test__handleMethodArgumentNotValidException__successMessageNull__shouldReturnCorrectResponse() {
    applicationMessages.add(new CustomMessage());
    HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
    MethodArgumentNotValidException methodArgumentNotValidException = mock(
        MethodArgumentNotValidException.class);
    List<FieldError> fieldErrors = new ArrayList<>();
    FieldError error = mock(FieldError.class);
    when(error.getField()).thenReturn("test");
    when(error.getDefaultMessage()).thenReturn("@test");
    fieldErrors.add(error);
    BindingResult bindingResult = mock(BindingResult.class);
    when(bindingResult.getFieldErrors()).thenReturn(fieldErrors);

    when(methodArgumentNotValidException.getBindingResult()).thenReturn(bindingResult);

    ResponseEntity result = exceptionHandlerController
        .handleMethodArgumentNotValidException(httpServletRequest, methodArgumentNotValidException);

    assertThat(result).isNotNull();
    assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(result.getBody()).isNotNull();
    assertThat(result.getBody()).isInstanceOf(GeneralResponse.class);
    assertThat(((GeneralResponse) result.getBody()).getStatus().getMessage())
        .isEqualTo(INVALID_REQUEST_ERROR.getDescription());
    assertThat(((GeneralResponse) result.getBody()).getStatus().getCode())
        .isEqualTo(INVALID_REQUEST_ERROR.getMessage());
  }

  @Test
  public void test__handleMethodArgumentNotValidException__successWithoutDefaultMessage__shouldReturnCorrectResponse() {
    applicationMessages.add(new CustomMessage());
    HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
    MethodArgumentNotValidException methodArgumentNotValidException = mock(
        MethodArgumentNotValidException.class);
    List<FieldError> fieldErrors = new ArrayList<>();
    FieldError error = mock(FieldError.class);
    when(error.getField()).thenReturn("test");
    when(error.getDefaultMessage()).thenReturn(null);
    fieldErrors.add(error);
    BindingResult bindingResult = mock(BindingResult.class);
    when(bindingResult.getFieldErrors()).thenReturn(fieldErrors);

    when(methodArgumentNotValidException.getBindingResult()).thenReturn(bindingResult);

    ResponseEntity result = exceptionHandlerController
        .handleMethodArgumentNotValidException(httpServletRequest, methodArgumentNotValidException);

    assertThat(result).isNotNull();
    assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(result.getBody()).isNotNull();
    assertThat(result.getBody()).isInstanceOf(GeneralResponse.class);
    assertThat(((GeneralResponse) result.getBody()).getStatus().getMessage())
        .isEqualTo(INVALID_REQUEST_ERROR.getDescription());
    assertThat(((GeneralResponse) result.getBody()).getStatus().getCode())
        .isEqualTo(INVALID_REQUEST_ERROR.getMessage());
  }

  private class CustomMessage implements ApplicationMessage {

    @Override
    public String getKey() {
      return "key";
    }

    @Override
    public String getMessage() {
      return "test";
    }

    @Override
    public String getDescription() {
      return "test";
    }

    @Override
    public int getStatusCode() {
      return 500;
    }

  }

}
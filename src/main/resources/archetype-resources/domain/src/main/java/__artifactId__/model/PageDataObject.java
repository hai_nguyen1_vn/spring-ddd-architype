#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.model;

import ${package}.${artifactId}.model.base.AbstractBaseDomainObject;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.Value;

@Value
@Builder
@Getter
@ToString
@EqualsAndHashCode(callSuper = true)
public class PageDataObject<T> extends AbstractBaseDomainObject {

  private T data;
  private PageObject page;
}

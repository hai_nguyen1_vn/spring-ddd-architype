#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.message;

public interface ApplicationMessage {

  String getKey();

  String getMessage();

  String getDescription();

  default int getStatusCode() {
    return 200;
  }

  default ApplicationMessage get(String key) {
    return this.getKey().equalsIgnoreCase(key) ? this : null;
  }
}

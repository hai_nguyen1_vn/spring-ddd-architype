#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Value;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Value
public class PageConstant {

  public static final String PAGE_INDEX = "page_index";
  public static final String PAGE_SIZE = "page_size";
  public static final String PAGING = "paging";
}

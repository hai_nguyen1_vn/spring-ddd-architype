#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.message;

import ${package}.${artifactId}.annotation.MessageSource;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
@MessageSource(name = "BadRequestMessage")
public enum BadRequestMessage implements ApplicationMessage {
  INVALID_REQUEST_ERROR("invalid_request", "Invalid request");

  private String message;
  private String description;

  @Override
  public String getKey() {
    return this.name();
  }

  @Override
  public int getStatusCode() {
    return 400;
  }
}

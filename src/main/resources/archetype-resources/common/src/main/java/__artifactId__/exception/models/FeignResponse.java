#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.${artifactId}.exception.models;

import java.util.Collection;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import lombok.Value;


@AllArgsConstructor
@Builder
@Getter
@Value
@ToString
public class FeignResponse {

  private Map<String, Object> body;
  private int statusCode;
  private Map<String, Collection<String>> headers;
}

